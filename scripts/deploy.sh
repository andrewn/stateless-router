#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

ROOT_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")/.." && pwd)

"${ROOT_DIR}/node_modules/.bin/wrangler" publish src/index.js --name stateless-router
