# stateless-router

This is an extremely simple POC of a stateless router of the type proposed in https://gitlab.com/gitlab-org/gitlab/-/issues/408507, and implemented with Cloudflare Workers.

See also discussion from FY24-Q1 Engineering Offsite: https://docs.google.com/document/d/1swxILpjhq3LIGCwF3emMW5Ktaa50PqyG-h2BPYteLwg/edit#bookmark=id.j0o2hkwh4sxx

This POC is really designed to create talking points of how an actual implementation might look.

## How Does it Work?

The code is all encapsulated in a single Cloudflare Service Worker: [`src/index.ts`](./src/index.ts)

![Basic Stateless Router Diagram](https://docs.google.com/drawings/d/e/2PACX-1vQj5uHJUdz9nwOV1d_1bfopTP1JoR5t56te9fLSa3ehgHq6gdDIOMTF68-LcNZBVyFV_g_6n4l29rFH/pub?w=1377&h=1026).

We don't have Cells deployed at present, but `stateless-router` leverages several well-known public GitLab instances, pretending that they are Cells for demonstration purposes.

When a request is received, the first element of the path (the "namespace") is extracted from the incoming URL and used for routing purposes.

1. If the path is `/gitlab-com/`: <https://stateless-router.andrew3371.workers.dev/gitlab-com/> the request is routed to `staging.gitlab.com`
1. If the path is `/xorgfoundation/`: <https://stateless-router.andrew3371.workers.dev/xorgfoundation/>, the request is routed to `gitlab.freedesktop.org`
1. If the path is `/videolan/`: <https://stateless-router.andrew3371.workers.dev/videolan/>, the request is routed to `code.videolan.org`.
1. If the path is `/debian/` <https://stateless-router.andrew3371.workers.dev/debian/>, the request is routed to `salsa.debian.org`.
1. Any other path routes to GitLab.com, eg: <https://stateless-router.andrew3371.workers.dev/gitlab-org/gitlab>.

At present, routing uses static configuration to perform host lookup for namespaces, but switching to a lookup service as described in <https://docs.gitlab.com/ee/architecture/blueprints/cells/proposal-stateless-router-with-routes-learning.html> would be possible in future, once a POC of this service is available.

## Challenges with using namespace routing

1. API calls are difficult to route.
    1. For the purposes of this demo, the stateless router falls back to the referer when API calls are being made.
    1. This approach is certainly not ideal as not all requests have a referer, and it may not be correct.
    1. In this demo, we can see that GraphQL queries are using an incorrect referer, making it difficult to correctly route these calls.
1. Authentication is not currently possible.
1. Cloudflare DDOS protection rejects the proxied requests.

## Hacking on stateless-router

### Preparing your Environment

1. Follow the developer setup guide at <https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/docs/developer-setup.md>.
1. Clone this project
1. Run `scripts/prepare-dev-env.sh`
