const namespaceMapping = new Map<string, string>(Object.entries({
  "gitlab-com": "staging.gitlab.com",
  "xorgfoundation": "gitlab.freedesktop.org",
  "videolan": "code.videolan.org",
  "debian": "salsa.debian.org"
}));

const useRefererBased = new Set<string>(["-", "api", "uploads", "assets"]);

async function fetchAndApply(request: Request): Promise<Response> {
  let url = new URL(request.url);
  let urlHostname = url.hostname;
  url.protocol = 'https:';

  let upstream = await routeRequestForPath(url, request);
  if (!upstream) {
    // Default to gitlab.com if all else fails...
    upstream = "gitlab.com";
  }

  url.host = upstream;
  url.port = '443';

  let method = request.method;
  let requestHeaders = request.headers;
  let newRequestHeaders = new Headers(requestHeaders);

  newRequestHeaders.set('Host', upstream);

  let originalResponse = await fetch(url.href, {
    method: method,
    headers: newRequestHeaders
  })

  let responseHeaders = originalResponse.headers;
  let newResponseHeaders = new Headers(responseHeaders);
  let status = originalResponse.status;

  newResponseHeaders.set('access-control-allow-origin', '*');
  newResponseHeaders.set('access-control-allow-credentials', 'true');
  newResponseHeaders.delete('content-security-policy');
  newResponseHeaders.delete('content-security-policy-report-only');
  newResponseHeaders.delete('clear-site-data');

  let response = new Response(originalResponse.body, {
    status,
    headers: newResponseHeaders
  })

  return response;
}

async function routeRequestForPath(url: URL, request: Request): Promise<string | undefined> {
  // For the purposes of this demo, we use static routing,
  // but ideally this could be done from a KV and/or cache
  // with a gitlab backing instance.
  const parts = url.pathname.split("/", 4)
  if (parts.length <= 2) {
    return;
  }

  let namespace = parts[1];

  // For /groups/, look at the next segment after the `/groups/` bit...
  if (namespace == "groups" && parts.length >= 3) {
    namespace = parts[2]
  }

  if (useRefererBased.has(namespace)) {
    return routeRequestForReferer(request);
  }

  return namespaceMapping.get(namespace)
}

// Common endpoint. Who sent us here?
async function routeRequestForReferer(request: Request): Promise<string | undefined> {
  const referer = request.headers.get("Referer");
  if (!referer) {
    return;
  }

  // This needs a lot more validation, and
  // is far, far, far from ideal
  let refererUrl = new URL(referer);
  const parts = refererUrl.pathname.split("/", 3)
  if (parts.length <= 2) {
    return;
  }

  let namespace = parts[1];

  return namespaceMapping.get(namespace)
}

export default {
  async fetch(request: Request): Promise<Response> {
    return fetchAndApply(request);
  },
};
